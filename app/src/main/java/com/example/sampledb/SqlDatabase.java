package com.example.sampledb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Database Activities
 * 
 */
public class SqlDatabase extends SQLiteOpenHelper {

	// DATABASE INFORMATION
	private static final String DB_NAME = "sampleDB.db";
	private static final int DB_VERSION = 2;

	// TABLE INFORMATTION
	public static final String ID = "_id";
	public static final String TABLE_NEWS = "newsData";
	public static final String NEWS_HEADLINE = "headline";
	public static final String NEWS_DATE = "date";

	// TABLE CREATION STATEMENT
	private static final String CREATE_TABLE = "create table " + TABLE_NEWS
			+ "(" + ID + " INTEGER primary key autoincrement, " + NEWS_HEADLINE
			+ " TEXT, " + NEWS_DATE + " TEXT);";

	public SqlDatabase(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NEWS);
		onCreate(db);
	}

}
