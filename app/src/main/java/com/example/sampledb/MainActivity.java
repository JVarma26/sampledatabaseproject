package com.example.sampledb;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {

	// instance of sql operation like insert delete
	private SqlOperation mSqlOperation;

	private EditText mNewsTitle;
	private ListView mNewsList;
	private boolean insertFlag, notInsertFlag;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// database operation
		mSqlOperation = new SqlOperation(this);

		// finding layout content
		mNewsTitle = (EditText) findViewById(R.id.news_Title);

		Button mSaveButton = (Button) findViewById(R.id.btn_save);

		mNewsList = (ListView) findViewById(R.id.news_list);

		// open database
		mSqlOperation.open();

		// show existing data
		refresh();

		// insert data into database
		mSaveButton.setOnClickListener(this);

		// list item clickable
		mNewsList.setLongClickable(true);
		mNewsList.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View view,
					int position, long id) {

				final long id_msg = id;
				// mSqlOperation.deleteData(id);
				// ////////////////

				// Alert Dialog
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						MainActivity.this);
				alertDialogBuilder.setTitle("Do you want to Delete");
				alertDialogBuilder
						.setMessage("Click yes to Delete !")

						.setCancelable(false)
						.setPositiveButton("Delete",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// if this button is clicked, close
										// current activity
										// MainActivity.this.finish();
										mSqlOperation.deleteData(id_msg);
										refresh();
									}
								})
						.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// if this button is clicked, just close
										// the dialog box and do nothing
										dialog.cancel();
									}
								});

				AlertDialog alertDialog = alertDialogBuilder.create();
				// // show it
				alertDialog.show();

				refresh();
				return false;
			}
		});
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// Close DB
		mSqlOperation.close();
	}

	// to refresh list item after sqlOperation
	public void refresh() {

		// Attach The Data From DataBase Into ListView Using Crusor
		// Adapter
		Cursor cursor = mSqlOperation.readData();

		// to check whether data exist or not
		if (cursor != null) {

			String[] from = new String[] { "headline", "date" };

			int[] to = new int[] { R.id.news_headline, R.id.news_time };

			SimpleCursorAdapter adapter = new SimpleCursorAdapter(
					MainActivity.this, R.layout.list_view, cursor, from, to, 0);

			mNewsList.setAdapter(adapter);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_save:
			String mgetTitle = mNewsTitle.getText().toString();
			if (mNewsTitle.getText().length() > 0) {

				mSqlOperation.insertData(mgetTitle, " ");
				mNewsTitle.setText("");
				insertFlag = true;
				notInsertFlag = false;
				Toast.makeText(getApplicationContext(), "Headline Entered",
						Toast.LENGTH_SHORT).show();

			} else {

				insertFlag = false;
				notInsertFlag = true;
				dialogBuilder(insertFlag, notInsertFlag);

			}
			refresh();
			break;

		default:
			break;
		}
	}

	public void dialogBuilder(Boolean insertFlag, Boolean notInsertFlag) {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				MainActivity.this);

		if (insertFlag) {

			alertDialogBuilder.setTitle("Enter HeadLine Inserted");
		} else if (notInsertFlag) {
			alertDialogBuilder.setTitle("Enter HeadLine");
		}
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton("Exit",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, close
								// current activity
								MainActivity.this.finish();
							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, just close
								// the dialog box and do nothing
								dialog.cancel();
							}
						});

		AlertDialog alertDialog = alertDialogBuilder.create();
		// // show it
		alertDialog.show();
	}

}
