package com.example.sampledb;

import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class SqlOperation {

	private SqlDatabase mSqlDatabase;
	private SQLiteDatabase database;

	private final Context mContext;

	public SqlOperation(Context c) {
		mContext = c;
	}

	/**
	 * open database connection
	 * 
	 * @return
	 * @throws SQLException
	 */
	public SqlOperation open() throws SQLException {

		mSqlDatabase = new SqlDatabase(mContext);
		database = mSqlDatabase.getWritableDatabase();
		return this;

	}

	/**
	 * close database connection
	 */
	public void close() {
		mSqlDatabase.close();
	}

	/**
	 * insert into database
	 * 
	 * @param name
	 * @param date
	 */
	public void insertData(String name, String date) {

		Date date1 = new Date(System.currentTimeMillis());

		ContentValues cv = new ContentValues();

		cv.put(SqlDatabase.NEWS_HEADLINE, name);
		cv.put(SqlDatabase.NEWS_DATE, date1.toString());

		database.insert(SqlDatabase.TABLE_NEWS, null, cv);
	}

	/**
	 * read data from database
	 * 
	 * 
	 * @return cursor
	 */
	public Cursor readData() {

		String[] allColumns = new String[] { SqlDatabase.ID,
				SqlDatabase.NEWS_HEADLINE, SqlDatabase.NEWS_DATE };

		Cursor c = null;
		c = database.query(SqlDatabase.TABLE_NEWS, allColumns, null, null,
				null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		return c;
	}

	/**
	 * delete data where id is provide
	 * @param id
	 */
	public void deleteData(long id) {

		// databse.delete(table, whereClause, whereArgs)
		database.delete(SqlDatabase.TABLE_NEWS, SqlDatabase.ID + "=" + id, null);
	}
}
